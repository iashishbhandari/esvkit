Pod::Spec.new do |s|
 s.name = "ESVKit"
 s.platform = :ios
 s.version = "0.1.1"
 s.license  = 'MIT'
 s.swift_version = '4.2'
s.summary = "A Swift framework for scrollable StackView"
 s.author = { "Ashish Bhandari" => "ashishbhandariplus@gmail.com" }
 s.homepage = "https://bitbucket.org/iashishbhandari"
 s.source = { :git => "https://bitbucket.org/iashishbhandari/esvkit.git", :tag => "#{s.version}"}

 s.ios.deployment_target = '11.0'

 s.source_files = "Source/*.swift"

 s.framework = "UIKit"
end
