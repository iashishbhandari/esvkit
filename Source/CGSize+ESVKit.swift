//
//  CGSize+ESVKit.swift
//  ESVKit
//
//  Created by Ashish Bhandari on 23/08/18.
//  Copyright © 2018 ashishplus. All rights reserved.
//

extension CGSize {
    
    static func +=(_ lhs: inout CGSize, _ rhs: CGSize) {
        lhs.width += rhs.width
        lhs.height += rhs.height
    }
}
