//
//  UIView+ESVKit.swift
//  ESVKit
//
//  Created by Ashish Bhandari on 23/08/18.
//  Copyright © 2018 ashishplus. All rights reserved.
//

extension UIView {
    
    func addSizeAnchors(onView view: UIView?, size: CGSize) {
        if let v = view {
            v.translatesAutoresizingMaskIntoConstraints = false
            var anchors = [NSLayoutConstraint]()
            anchors.append(v.widthAnchor.constraintEqualTo(constant: size.width, identifier: "widthIdentifier"))
            anchors.append(v.heightAnchor.constraintEqualTo( constant: size.height, identifier: "heightIdentifier"))
            NSLayoutConstraint.activate(anchors)
        }
    }
    
    func addSafeAreaEdgeAnchors(onView view: UIView?, edgeInsets: UIEdgeInsets) {
        if let v = view {
            v.translatesAutoresizingMaskIntoConstraints = false
            var anchors = [NSLayoutConstraint]()
            anchors.append(v.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: edgeInsets.top))
            anchors.append(v.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: edgeInsets.left))
            anchors.append(v.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: edgeInsets.right))
            anchors.append(v.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: edgeInsets.bottom))
            NSLayoutConstraint.activate(anchors)
        }
    }
    
    func constraint(withIdentifier: String) -> NSLayoutConstraint? {
        return self.constraints.filter{ $0.identifier == withIdentifier }.first
    }
}

