//
//  NSLayoutDimension+ESVKit.swift
//  ESVKit
//
//  Created by Ashish Bhandari on 23/08/18.
//  Copyright © 2018 ashishplus. All rights reserved.
//

extension NSLayoutDimension {
    
    func constraintEqualTo(constant: CGFloat, identifier: String) -> NSLayoutConstraint {
        let constraint = self.constraint(equalToConstant: constant)
        constraint.identifier = identifier
        return constraint
    }
}
