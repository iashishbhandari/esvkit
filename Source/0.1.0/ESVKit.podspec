Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '11.0'
s.swift_version = '4.2'
s.name = "ESVKit"
s.summary = "ESVKit is amalgamation of UIStackView and UIScrollView to conveniently build a responsive UI."
s.requires_arc = true

s.version = "0.1.0"

s.license  = 'MIT'

s.author = { "Ashish Bhandari" => "ashishbhandariplus@gmail.com" }

s.homepage = "https://bitbucket.org/iashishbhandari/esvkit"

s.source = { :git => "https://bitbucket.org/iashishbhandari/esvkit.git", :tag => "#{s.version}"}

s.framework = "UIKit"

s.source_files = "ESVKit/*.{h,swift}"

end
