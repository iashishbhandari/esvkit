//
//  EasyStackView.swift
//  ESVKit
//
//  Created by Ashish Bhandari on 21/08/18.
//  Copyright © 2018 ashishplus. All rights reserved.
//

import UIKit

public class EasyStackView: UIStackView {
    
    public enum ScrollConfiguration {
        case none
        case standardVertical
        case standardHorizontal
        case angularVertical(angle: CGFloat)
        case angularHorizontal(angle: CGFloat)
    }
    
    public let scrollView: UIScrollView = {
        let scrollV = UIScrollView(frame: CGRect.zero)
        scrollV.isScrollEnabled = true
        scrollV.isUserInteractionEnabled = true
        scrollV.showsHorizontalScrollIndicator = false
        scrollV.showsVerticalScrollIndicator = false
        return scrollV
    }()
    
    public var configuration: ScrollConfiguration = .none {
        didSet {
            switch configuration {
            case .standardHorizontal:
                axis = .horizontal
                alignment = .center
                distribution = .equalSpacing
                spacing = 5.0
            case .standardVertical:
                axis = .vertical
                alignment = .center
                distribution = .equalSpacing
                spacing = 5.0
            case .angularHorizontal:
                axis = .horizontal
                alignment = .center
                distribution = .equalSpacing
                spacing = 5.0
                break
            case .angularVertical:
                axis = .vertical
                alignment = .center
                distribution = .equalSpacing
                spacing = 5.0
                break
            case .none: break
            }
        }
    }
    
    public func embedTo(view: UIView?, edgeInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)) {
        if let v = view {
            scrollView.contentSize += CGSize(width: edgeInsets.left + edgeInsets.right, height: edgeInsets.top + edgeInsets.bottom)
            v.addSubview(scrollView)
            v.addSafeAreaEdgeAnchors(onView: scrollView, edgeInsets: edgeInsets)
            // StackView alignments
            scrollView.addSubview(self)
            translatesAutoresizingMaskIntoConstraints = false
            switch axis {
            case .horizontal:
                leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: edgeInsets.left).isActive = true
            case .vertical:
                topAnchor.constraint(equalTo: scrollView.topAnchor, constant: edgeInsets.top).isActive = true
            }
            switch configuration {
            case .angularVertical(let angle), .angularHorizontal(let angle):
                scrollView.transform = scrollView.transform.rotated(by: angle )
            default: break
            }
        }
    }
    
    public func addSubView(_ view: UIView?, atIndex index: Int =         Int.max) {
        guard let view = view else {
            return
        }
        if index < arrangedSubviews.count {
            insertArrangedSubview(view, at: index)
        } else {
            addArrangedSubview(view)
        }
        addSizeAnchors(onView: view, size: view.bounds.size)
        switch axis {
        case .horizontal:
            scrollView.contentSize.width += (view.bounds.width + spacing)
        case .vertical:
            scrollView.contentSize.height += (view.bounds.height + spacing)
        }
    }
    
    public func removeSubView(_ view: UIView?) {
        guard let view = view else {
            return
        }
        removeArrangedSubview(view)
        view.removeFromSuperview()
        switch axis {
        case .horizontal:
            scrollView.contentSize.width -= (view.bounds.width + spacing)
        case .vertical:
            scrollView.contentSize.height -= (view.bounds.height + spacing)
        }
    }
    
    public func resizeSubView(_ view: UIView?, newSize: CGSize) {
        if let v = view {
            v.constraint(withIdentifier: "widthIdentifier")?.constant = newSize.width
            v.constraint(withIdentifier: "heightIdentifier")?.constant = newSize.height
            switch axis {
            case .horizontal:
                scrollView.contentSize.width -= (v.bounds.width - newSize.width)
            case .vertical:
                scrollView.contentSize.height -= (v.bounds.height - newSize.height)
            }
        }
    }
}


